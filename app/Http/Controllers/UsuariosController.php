<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UsuariosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function PorNombre(Request $request){
        $nombre = $request->get('nombre');      
        $usuarios=User::where('username','like',"%$nombre%")->orderBy('id', 'DESC')->get();

        return view('pages.users.index',compact('usuarios'));
    }
    public function index()
    {
        $usuarios = User::paginate(5); 
        return view('pages.users.index', compact('usuarios'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        return view("pages.users.create");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $usuarios = User::create($request->all());
        if($request->has('password')){
            $usuarios->password = Hash::make($request->password);
        } 
        $usuarios->save();
        return redirect()->route('usuarios')->with('flash','Su usuario ha sido guardado satisfactoriamente.');
    }

    public function registro(Request $request)
    {
        $usuarios =  User::create([
            'nombre' => $request['nombre'],
            'email' => $request['email'],
            'password' => Hash::make($request['password']),
        ]);
        if($request->has('password')){
            $usuarios->password = Hash::make($request->password);
        } 
        $usuarios->save();
        return redirect()->route('Usuarios')->with('flash','Su usuario ha sido guardado satisfactoriamente.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $usuarios = User::findOrFail($id);
        return view('pages.users.edit', compact('usuarios'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $usuarios =  User::findOrFail($id);
        $usuarios->fill($request->all());
        if($request->has('password')){
            $usuarios->password = Hash::make($request->password);
        } 
        $usuarios->save();
        return redirect()->route('usuarios')->with('flash','Su usuario ha sido actualizado con Exito.');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $usuarios = User::findOrFail($id);
        $usuarios->delete();
        return redirect()->route('usuarios')->with('flash','Su usuario ha sido eliminado con Exito.');
    }
}
