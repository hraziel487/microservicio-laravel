<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class VideojuegosController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */


     public function index()
    {
        $response = Http::get('https://api.racielhernandez.com/public/videojuegos');
        $datos = $response->json();

        $videojuegos = [];

        foreach ($datos['data'] as $videojuego) {
            
            
            $videojuegos[] = [
                'id'=>$videojuego['id'],
                'nombre'=>$videojuego['Nombre'],
                'descripcion'=>$videojuego['Descripcion'],
                'lanzamiento'=>$videojuego['Lanzamiento'],
                'portada'=> $videojuego['Portada'],
                'compania'=> $videojuego['Compania'],
                'consolas'=> $videojuego['Consolas'],
            ];
        }

        
        return view('pages.index', ['videojuegos'=>$videojuegos]);

        /*
        $response = $this->cliente->get('videojuegos');
        $data = $response->getBody()->getContents();
        
        return view('Pages.videojuegos.index', ['videojuegos' => json_decode($data)]);
        */
    }
/*        $response = Http::get('https://api.racielhernandez.com/public/videojuegos'); 
        $data = $response->json();
        return view('Pages.videojuegos.index', ['videojuegos' => $data]);*/
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request)
    {
        $response = Http::post('https://api.racielhernandez.com/public/videojuegos', [
            'Nombre' => $request->input('nombre'),
            'Descripcion' => $request->input('descripcion'),
            'Lanzamiento' => $request->input('lanzamiento'),
            'Portada' => $request->input('portada'),
            'Compania' => $request->input('compania'),
            'Consolas' => $request->input('consolas'),
        ]);

        $data = $response->json();

        return view('pages.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $response = Http::get('https://api.racielhernandez.com/public/videojuegos/' .$id);
        $videojuego = $response->json();
    
        return view('pages.edit', ['videojuegos'=>$videojuego]);
        }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $response = Http::put('https://api.racielhernandez.com/public/videojuegos/'.$id, [
            'Nombre' => $request->input('nombre'),
            'Descripcion' => $request->input('descripcion'),
            'Lanzamiento' => $request->input('lanzamiento'),
            'Portada' => $request->file('portada'),
            'Compania' => $request->input('compania'),
            'Consolas' => $request->input('consolas'),
        ]);

        return redirect()->route('videojuegos.index')->with('flash','Su usuario ha sido actualizado con Exito.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $response = Http::delete('https://api.racielhernandez.com/public/videojuegos/'.$id);

        $data = $response->json();

        return view('pages.index');
     }
}
