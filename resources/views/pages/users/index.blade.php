@extends('layouts.app')

@section('content')
@include('layouts.navbars.auth.topnav', ['title' => 'User Management'])
<div class="row mt-4 mx-4">
    <div class="col-12">
        <div class="alert alert-light" role="alert">
            CRUD API Videojuegos <strong>en Laravel 9
        </div>
<div class="container-fluid mt--7">
<div class="page-inner mt--5">
    <div class="row">
        <div class="col">
            <div class="card shadow">
                <div class="card-header border-0">
                    <div class="row align-items-center">
                        <div class="col-8">
                            <h3 class="mb-0">Usuarios</h3>
                        </div>
                        <div class="col-4 text-right">
                            <a href="{{ route('usuarios.create') }}" class="btn btn-sm btn-primary">Agregar usuarios</a>
                        </div>
                    </div>
                </div>

                <div class="col-12">
                    <!--Filtro por Nombre--->
               
                <form action="/nombre"  method="GET"  style="display: inline-block;"  class="form-group  col-md-5" >
                <label style="margin-right: 0.2rem;">Nombre:  
                   <input type="text" class="form-control border border-secondary" style="display: inline-block; margin-right: 0.2rem;" placeholder="Escribir Userme" name="nombre"> 
                   <input type="submit"  class="btn btn-sm btn-primary"  value="filtrar"></label>
    </form>
    <!-- Fin de filtro -->
                </div>

                <div class="table-responsive">
                    <table class="table align-items-center table-flush">
                        <thead class="thead-light">
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Nombre</th>
                                <th scope="col">Email</th>
                                <th scope="col">Password</th>
                                <th scope="col">Fecha</th>
                                <th scope="col">Acciones</th>
                            </tr>
                        </thead>
                        @foreach ($usuarios as $orden=>$users)
                        <tbody>
                            <tr>
                                <td>{{++$orden}}</td>
                                <td>{{$users->username}}</td>
                                <td>{{$users->email}}</td>
                                <td>{{$users->password}}</td>

                                <td>{{$users->created_at}}</td>
                                <td>



                                    <form action="{{route("usuarios.destroy", $users->id)}}" method="POST">
                                        @method("DELETE")
                                        @csrf
                                        <a class="btn btn-primary" href="{{ route('usuarios.edit', $users->id ) }}">Editar</a>
                                        <button class="btn btn-secondary" type="submit">Eliminar</button>

                                    </form>


                                </td>
                            </tr>
                        </tbody>

                        @endforeach
                    </table>
                </div>

            </div>
        </div>

    </div>


</div>

@endsection