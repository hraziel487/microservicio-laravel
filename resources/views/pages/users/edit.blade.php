@extends('layouts.app')

@section('content')
@include('layouts.navbars.auth.topnav', ['title' => 'User Management'])
<div class="header bg-gradient-primary pb-8 pt-5 pt-md-8"></div>
<div class="container-fluid mt--7" style="">
    <div class="card bg-white shadow">
        <div class="card-header bg-white border-0">
            <div class="row align-items-center">
                <h1 class="mb-0">{{ __('Agregar Usuarios') }}</h1>
            </div>
        </div>
        <div class="card-body">
            <form method="post" action="{{ route('usuarios.update',$usuarios->id ) }}" autocomplete="off" enctype="multipart/form-data">
                @csrf
                @method('put')

                <h6 class="heading-small text-muted mb-4">{{ __('Editar Usuarios') }}</h6>
                
                @if (session('status'))
                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                        {{ session('status') }}
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                @endif

                    <div class="form-group{{ $errors->has('nombre') ? ' has-danger' : '' }}">
                        <label class="form-control-label" for="input-status">{{ __('Nombre de usuario') }}</label>
                        <input type="text" name="username" id="input-status" class="form-control form-control-alternative{{ $errors->has('status') ? ' is-invalid' : '' }}" placeholder="{{ __('Nombre') }}" value="{{$usuarios->username}}" required>

                        @if ($errors->has('nombre'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('nombre') }}</strong>
                            </span>
                        @endif
                    </div>

                    <div class="form-group{{ $errors->has('nombre') ? ' has-danger' : '' }}">
                        <label class="form-control-label" for="input-status">{{ __('Nombre') }}</label>
                        <input type="text" name="firstname" id="input-status" class="form-control form-control-alternative{{ $errors->has('status') ? ' is-invalid' : '' }}" placeholder="{{ __('Nombre') }}" value="{{$usuarios->firstname}}" required>

                        @if ($errors->has('nombre'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('nombre') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group{{ $errors->has('nombre') ? ' has-danger' : '' }}">
                        <label class="form-control-label" for="input-status">{{ __('Apellido') }}</label>
                        <input type="text" name="lastname" id="input-status" class="form-control form-control-alternative{{ $errors->has('status') ? ' is-invalid' : '' }}" placeholder="{{ __('Nombre') }}" value="{{$usuarios->lastname}}" required>

                        @if ($errors->has('nombre'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('nombre') }}</strong>
                            </span>
                        @endif
                    </div>

                    <div class="form-group{{ $errors->has(' nickname   ') ? ' has-danger' : '' }}">
                        <label class="form-control-label" for="input-status">{{ __(' Direccion  ') }}</label>
                        <input type="text" name="address" id="input-status" class="form-control form-control-alternative{{ $errors->has('address ') ? ' is-invalid' : '' }}" placeholder="{{ __('address ') }}" value="{{$usuarios->address}}" required>

                        @if ($errors->has('address  '))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('address  ') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group{{ $errors->has(' city   ') ? ' has-danger' : '' }}">
                        <label class="form-control-label" for="input-status">{{ __(' Ciudad  ') }}</label>
                        <input type="text" name="city" id="input-status" class="form-control form-control-alternative{{ $errors->has('city ') ? ' is-invalid' : '' }}" placeholder="{{ __('city ') }}" value="{{$usuarios->city}}" required>

                        @if ($errors->has('city  '))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('city  ') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group{{ $errors->has(' city   ') ? ' has-danger' : '' }}">
                        <label class="form-control-label" for="input-status">{{ __(' Pais ') }}</label>
                        <input type="text" name="country" id="input-status" class="form-control form-control-alternative{{ $errors->has('city ') ? ' is-invalid' : '' }}" placeholder="{{ __('Pais ') }}" value="{{$usuarios->country}}" required>

                        @if ($errors->has('country  '))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('country  ') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group{{ $errors->has(' postal   ') ? ' has-danger' : '' }}">
                        <label class="form-control-label" for="input-status">{{ __(' Postal ') }}</label>
                        <input type="text" name="postal" id="input-status" class="form-control form-control-alternative{{ $errors->has('city ') ? ' is-invalid' : '' }}" placeholder="{{ __('Postal ') }}" value="{{$usuarios->postal}}" required>

                        @if ($errors->has('postal  '))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('postal  ') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group{{ $errors->has(' sobre   ') ? ' has-danger' : '' }}">
                        <label class="form-control-label" for="input-status">{{ __(' Sobre ti ') }}</label>
                        <input type="text" name="about" id="input-status" class="form-control form-control-alternative{{ $errors->has('city ') ? ' is-invalid' : '' }}" placeholder="{{ __('Sobre ') }}" value="{{$usuarios->about}}" required>

                        @if ($errors->has('about  '))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('about  ') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group{{ $errors->has('  	email   ') ? ' has-danger' : '' }}">
                        <label class="form-control-label" for="input-status">{{ __('  	Email  ') }}</label>
                        <input type="email" name="email" id="input-status" class="form-control form-control-alternative{{ $errors->has(' 	email ') ? ' is-invalid' : '' }}" placeholder="{{ __(' 	email ') }}" value="{{$usuarios->email}}" required>

                        @if ($errors->has(' 	email  '))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first(' 	email  ') }}</strong>
                            </span>
                        @endif
                    </div>

                    <div class="form-group{{ $errors->has('  	password   ') ? ' has-danger' : '' }}">
                        <label class="form-control-label" for="input-status">{{ __('  	Password  ') }}</label>
                        <input type="password" name="password" id="input-status" class="form-control form-control-alternative{{ $errors->has(' 	password ') ? ' is-invalid' : '' }}" placeholder="{{ __(' 	password ') }}" value="" required>

                        @if ($errors->has(' 	password  '))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first(' 	password  ') }}</strong>
                            </span>
                        @endif
                    </div>

                    <div class="col-md-6 center">
                        <button type="submit" class="btn btn-primary btn-lg btn-block">{{ __('Guardar') }}</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
    </div>
@endsection
