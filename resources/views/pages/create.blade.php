@extends('layouts.app', ['class' => 'g-sidenav-show bg-gray-100'])

@section('content')
    @include('layouts.navbars.auth.topnav', ['title' => 'Profile'])
   
    <div class="container-fluid py-4">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <p class="text-uppercase text-sm">Crear VideoJuego</p>
                        <div class="row">
                            <div class="col-md-6">
                            <form method="post" action="{{route('videojuegos.store')}}"  autocomplete="off" enctype="multipart/form-data">
                            @csrf
                @method('post')
                                <div class="form-group">
                                    <label for="example-text-input" class="form-control-label">Nombre</label>
                                    <input class="form-control" type="text" name="nombre" id="nombre" >
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="example-text-input" class="form-control-label">Descripcion</label>
                                    <input class="form-control" type="text" name="descripcion" value="des">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="example-text-input" class="form-control-label">Lanzamiento</label>
                                    <input class="form-control" type="date" name="lanzamiento" value="">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="example-text-input" class="form-control-label">Portada</label>
                                    <input class="form-control" type="file" name="portada">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="example-text-input" class="form-control-label">Compañia</label>
                                    <input class="form-control" type="text" name="compania" value="comp">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="example-text-input" class="form-control-label">Consolas</label>
                                    <input class="form-control" type="text" name="consolas" value="consol">
                                </div>
                            </div>
                            <div class="row" style="margin-top: 20px">
                            <div class="col-md-6 center">
                                <button type="submit" class="btn btn-primary btn-lg btn-block">{{ __('Guardar') }}</button>
                            </div>
                        </div>
                    </form>
                        </div>
                        
    </div>
@endsection
