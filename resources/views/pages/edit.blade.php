@extends('layouts.app')

@section('content')
@include('layouts.navbars.auth.topnav', ['title' => 'User Management'])
   
    <div class="container-fluid py-4">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <p class="text-uppercase text-sm">Editar VideoJuego</p>
                        <div class="row">
                            <div class="col-md-6">
                            @foreach($videojuegos as $videojuego)
                            <form method="post" action="{{route('videojuegos.update',$videojuego['id'])}}"  autocomplete="off" enctype="multipart/form-data">
                            @csrf
                @method('PUT')
                                <div class="form-group">
                                    <label for="example-text-input" class="form-control-label">Nombre</label>
                                    <input class="form-control" type="text" name="nombre" value="{{$videojuego['Nombre']}}" >
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="example-text-input" class="form-control-label">Descripcion</label>
                                    <input class="form-control" type="text" name="descripcion" value="{{$videojuego['Descripcion']}}">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="example-text-input" class="form-control-label">Lanzamiento</label>
                                    <input class="form-control" type="date" name="lanzamiento" value="{{$videojuego['Lanzamiento']}}">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="example-text-input" class="form-control-label">Portada</label>
                                    <input class="form-control" type="file" name="portada">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="example-text-input" class="form-control-label">Compañia</label>
                                    <input class="form-control" type="text" name="compania" value="{{$videojuego['Compania']}}">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="example-text-input" class="form-control-label">Consolas</label>
                                    <input class="form-control" type="text" name="consolas" value="{{$videojuego['Consolas']}}">
                                </div>
                            </div>
                            <div class="row" style="margin-top: 20px">
                            <div class="col-md-6 center">
                                <button type="submit" class="btn btn-primary btn-lg btn-block">{{ __('Editar') }}</button>
                            </div>
                        </div>
                    </form>
                    
                    @endforeach
                        </div>
                        
    </div>
@endsection
