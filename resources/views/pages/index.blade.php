@extends('layouts.app')

@section('content')
@include('layouts.navbars.auth.topnav', ['title' => 'User Management'])
<div class="row mt-4 mx-4">
    <div class="col-12">
        <div class="alert alert-light" role="alert">
            CRUD API Videojuegos <strong>en Laravel 9
        </div>
        <div class="card mb-4">
            <div class="card-header pb-0">
                <h6>Video Juegos</h6>
            </div>
            <div class="card-body px-0 pt-0 pb-2">
                <div class="table-responsive p-0">
                    <table class="table align-items-center mb-0">
                        <thead>
                            <tr>
                                <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Nombre</th>
                                <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">Descripcion
                                </th>
                                <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">Lanzamiento
                                </th>
                                <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">Portada
                                </th>
                                <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">Compañia
                                </th>
                                <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">Consolas
                                </th>
                                <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
                                    Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($videojuegos as $videojuego)
                            <tr>
                                <td>
                                    <div class="d-flex px-3 py-1">

                                        <div class="d-flex flex-column justify-content-center">
                                            <h6 class="mb-0 text-sm">{{$videojuego['nombre']}}</h6>
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <p class="text-sm font-weight-bold mb-0">{{$videojuego['descripcion']}}</p>
                                </td>
                                <td class="align-middle text-center text-sm">
                                    <p class="text-sm font-weight-bold mb-0">{{$videojuego['lanzamiento']}}</p>
                                </td>
                                <td>
                                    <div class="d-flex px-3 py-1">
                                        <div>
                                            <img src="https://api.racielhernandez.com/Portada/{{ $videojuego['portada']}}"" class=" avatar me-3" alt="image">
                                        </div>

                                    </div>
                                </td>
                                <td class="align-middle text-center text-sm">
                                    <p class="text-sm font-weight-bold mb-0">{{$videojuego['compania']}}</p>
                                </td>
                                <td class="align-middle text-center text-sm">
                                    <p class="text-sm font-weight-bold mb-0">{{$videojuego['consolas']}}</p>
                                </td>
                                <td>
                                    <a class="btn btn-primary" href="{{ url('videojuegos/'. $videojuego['id'] . '/edit') }}">Editar</a>
                                </td>

                                <td>
                                    <form action="{{ url('videojuegos', $videojuego['id']) }}" method="POST">
                                        @csrf
                                        @method("DELETE")

                                        <button class="btn btn-secondary" type="submit">Eliminar</button>

                                    </form>

                                </td>

                            </tr>

                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection